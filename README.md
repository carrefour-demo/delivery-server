# Delivery Service

This project is a Spring Boot application that provides a delivery service, allowing customers to choose a delivery method and book delivery time slots.

## Table of Contents

- [Introduction](#introduction)
- [Features](#features)
- [Architecture](#architecture)
- [Technologies](#technologies)
- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Installation](#installation)
  - [Running with Docker](#running-with-docker)
- [API Documentation](#api-documentation)
- [Notes](#notes)
- [Propositions](#propositions)
  - [Security Enhancement](#security-enhancement)
  - [Usage of Non-blocking Solution](#usage-of-non-blocking-solution)
  - [Caching Solution](#caching-solution)
  - [Event-driven Architecture](#event-driven-architecture)
  - [CI/CD](#cicd)
  - [Tests](#tests)

## Introduction

The Delivery Service is designed to provide a platform for customers to book delivery time slots based on their preferences. The application supports various delivery methods and allows customers to select their preferred delivery date and time slot.

## Features

- Customers can choose from different delivery methods: `DRIVE`, `DELIVERY`, `DELIVERY_TODAY`, `DELIVERY_ASAP`.
- Customers can book delivery time slots based on availability.
- Time slots are specific to delivery methods and can be booked by multiple customers.

## Architecture

![architecture](https://miro.medium.com/v2/resize:fit:700/1*FQRODMcECQGFv_r8ghe7nQ.png)

The architecture of this project follows a typical client-server architecture pattern, with Angular as the client-side application framework and Spring Boot as the server-side framework.

- **DAO**: This tier performs the data, it describes the entities and the access logic
- **INTEGRATION**: This component includes all the interactions/integrations without a specific binding to the application’s context in order to guarantee a very high reusability
- **BUSINESS LOGIC**: This tier process the data without entering into the details of how the database is structured or what integration is underneath.
- **API**: This tier handle the logic of presentation, it represents the entry point of the server of our application.
- **FRONTEND**: This tier is responsible for providing the user interface and handling user interactions in a web application.

## Technologies

- **Spring Boot 3.3.0**: Backend framework.
- **Spring Data JPA**: Data persistence.
- **H2 Database**: In-memory database for development and testing.
- **Spring Security OAuth2**: Authentication and authorization.
- **Spring HATEOAS**: Hypermedia support for RESTful APIs.
- **SpringDoc OpenAPI**: API documentation.
- **Lombok**: Reducing boilerplate code with annotations.

## Getting Started

### Prerequisites

Before running the application, ensure you have the following installed:

- **Java Development Kit (JDK) 21 or later**
- **Apache Maven**
- **Git**


### Installation

1. **Clone the repository:**

   ``` git clone https://gitlab.com/carrefour-demo/delivery-server.git ```

2. **Build the project using Maven:**

   ```  mvn clean install ```

3. **Run the application:**

    ```  mvn spring-boot:run ```

The application will start on http://localhost:8080.

### Running with Docker

1. **Build Docker image:**

``` docker build -t delivery-service . ```

2. **Run Docker containers with Docker Compose:**

``` docker-compose up ```

This will start the application along with the necessary dependencies like the database and the front-end.

## API Documentation

The API documentation is available via Swagger. You can access it at the following URL: [http://localhost:8080/carrefour/swagger-ui](http://localhost:8080/carrefour/swagger-ui).

### Security

By default, all the APIs are protected by a security filter. To access the APIs, you need to authenticate and obtain a token.

### Steps to Access the APIs:

1. **Log In with Test User Credentials:**

   - **Username**: `test`
   - **Password**: `0000`

2. **Obtain the Token:**

   Upon successful login, you will receive an authentication token.

3. **Authorize in Swagger:**

   - Click on the "Authorize" button in the Swagger UI.
   - Enter the token you received in the "Value" field.

   This will automatically inject the token into every API call, allowing you to access the secured endpoints.

### Available APIs

- **Delivery Methods**
  - `GET /api/delivery/methods` - Retrieve all available delivery methods.

- **Time Slots**
  - `GET /api/timeslots` - Retrieve available time slots based on delivery method, with pagination.
  - `POST /api/timeslots/book` - Book a time slot.

- ** Auth Api**
  - `POST /api/auth/login` - Log In with User Credentials

## Notes

- Added an initializer to create a test user and generate a week of available time slots.
- Feature branches were not deleted after usage for traceability purposes.
- A mapper has to be added to replace boilerplate mapping code.
- Customer ID could be extracted from Authorization
- Time slot periods are dynamic to be changed according to the delivery team's capacity


## Propositions

### Security Enhancement : 

- **OAuth2 Client Integration**: We can consider integrating an OpenID Connect provider like Keycloak to handle Authentication and Authorization. By implementing OAuth2, we can secure the API with tokens and define permission rules for accessing resources.

### Usage of non blocking solution : 

- **Reactive Programming**: We can Consider adopting Spring WebFlux, to implement non-blocking solutions for the time slot service. We can handle multiple concurrent requests for booking delivery time slots. This is particularly beneficial in scenarios where there is high demand for delivery time slots or when integrating with external services for real-time updates on delivery statuses.

### Caching Solution :

- **Spring Cache**: We can Integrate Spring Cache with an in-memory cache provider such as Ehcache, Caffeine, or Hazelcast to provide a simple abstraction for caching.

- **Redis**: We can use Redis in-memory data store, for caching. Redis is well-suited for scenarios requiring fast access to frequently accessed data.

### Event driven architecture :

- **Kafka**: We can enable event-driven communication to decouple the components of the system. for example we can use Kafka topics allow different parts of the application to produce and consume events asynchronously.This means that actions such as booking a time slot can produce events that other parts of the system can consume and react to.in this case, the service that handles booking does not need to know about the consumers which are the services that handle post-booking processes such as sending notifications.

- **CQRS Axon**: We can also implement Command Query Responsibility Segregation (CQRS) with Event Sourcing to separate the read and write operations. CQRS allows us to use different models for updating data and reading data, while Event Sourcing ensures that every change to the state of an application is captured as a sequence of events.

### CI/CD : 

- **GitLab CI/CD and Kubernetes** : We can leverage GitLab CI/CD combined with Kubernetes configuration to automate tests, builds, and deployment processes.

### Tests :

- **Cypress** : For E2E testing we can use a combination of Cypress framework and GitLab CI/CD pipeline to ensure tests run automatically on every commit and deployment.

- **JUnit and Mockito** : For unit testing and integration testing we can use JUnit and Mockito which integrate well in the Java/Spring ecosystem.
