package com.carrefour.demo.delivery.api;

import com.carrefour.demo.delivery.application.DeliveryService;
import com.carrefour.demo.delivery.core.delivery.DeliveryMethod;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpHeaders;
import org.springframework.test.web.servlet.MockMvc;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.jwt;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.hamcrest.CoreMatchers.*;



@SpringBootTest
@AutoConfigureMockMvc
public class DeliveryApiTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private DeliveryService deliveryService;

    @Test
    public void testGetAllDeliveryMethods() throws Exception {

        List<DeliveryMethod> methods = Arrays.asList(DeliveryMethod.DRIVE, DeliveryMethod.DELIVERY, DeliveryMethod.DELIVERY_TODAY, DeliveryMethod.DELIVERY_ASAP);
        List<String> methodNames = methods.stream().map(DeliveryMethod::name).collect(Collectors.toList());

        Mockito.when(deliveryService.getAllDeliveryMethodNames()).thenReturn(methodNames);

        mvc.perform(get("/api/delivery/methods").with(jwt().jwt(jwt -> jwt.claim("scope", "read"))))
                .andExpect(status().isOk())
                .andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaTypes.HAL_JSON_VALUE))
                .andExpect(jsonPath("$._embedded.stringList[0]", is("DRIVE")))
                .andExpect(jsonPath("$._embedded.stringList[1]", is("DELIVERY")))
                .andExpect(jsonPath("$._embedded.stringList[2]", is("DELIVERY_TODAY")))
                .andExpect(jsonPath("$._embedded.stringList[3]", is("DELIVERY_ASAP")));
    }

    @Test
    public void testGetAllDeliveryMethodsUnauthenticated() throws Exception {
        mvc.perform(get("/api/delivery/methods"))
                .andExpect(status().isUnauthorized());
    }
}

