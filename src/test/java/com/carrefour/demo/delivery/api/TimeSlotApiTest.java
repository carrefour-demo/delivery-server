package com.carrefour.demo.delivery.api;

import com.carrefour.demo.delivery.application.TimeSlotService;
import com.carrefour.demo.delivery.core.delivery.TimeSlot;
import com.carrefour.demo.delivery.core.delivery.DeliveryMethod;

import com.carrefour.demo.delivery.core.user.User;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.jwt;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.LocalDateTime;
import java.util.Arrays;


@SpringBootTest
@AutoConfigureMockMvc
public class TimeSlotApiTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private TimeSlotService timeSlotService;

    @Test
    public void testGetAvailableTimeSlots() throws Exception {
        TimeSlot timeSlot1 = new TimeSlot(1L, LocalDateTime.of(2024, 6, 16, 13, 0), LocalDateTime.of(2024, 6, 16, 14, 0), null, false, null);
        TimeSlot timeSlot2 = new TimeSlot(2L, LocalDateTime.of(2024, 6, 16, 14, 0), LocalDateTime.of(2024, 6, 16, 15, 0), null, false, null);

        Page<TimeSlot> timeSlotsPage = new PageImpl<>(Arrays.asList(timeSlot1, timeSlot2));



        Mockito.when(timeSlotService.getAvailableTimeSlots(DeliveryMethod.DELIVERY, PageRequest.of(0, 2)))
                .thenReturn(timeSlotsPage);


        String expectedJson = "["
                + "{"
                + "\"id\":1,"
                + "\"startTime\":\"2024-06-16T13:00:00\","
                + "\"endTime\":\"2024-06-16T14:00:00\","
                + "\"deliveryMethod\":null,"
                + "\"booked\":false,"
                + "\"bookedByUserId\":null,"
                + "\"links\":[{\"rel\":\"self\",\"href\":\"http://localhost/api/time-slots?deliveryMethod=DELIVERY&page=0&size=2\"}]"
                + "},"
                + "{"
                + "\"id\":2,"
                + "\"startTime\":\"2024-06-16T14:00:00\","
                + "\"endTime\":\"2024-06-16T15:00:00\","
                + "\"deliveryMethod\":null,"
                + "\"booked\":false,"
                + "\"bookedByUserId\":null,"
                + "\"links\":[{\"rel\":\"self\",\"href\":\"http://localhost/api/time-slots?deliveryMethod=DELIVERY&page=0&size=2\"}]"
                + "}"
                + "]";

        mvc.perform(get("/api/time-slots")
                        .param("deliveryMethod", "DELIVERY")
                        .param("page", "0")
                        .param("size", "2")
                        .with(jwt().jwt(jwt -> jwt.claim("scope", "read"))))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(expectedJson));
    }

    @Test
    public void testBookTimeSlot() throws Exception {

        User user = new User();
        user.setUserId("01");

        TimeSlot timeSlot = new TimeSlot(1L, LocalDateTime.of(2024, 6, 16, 13, 0), LocalDateTime.of(2024, 6, 16, 14, 0),  DeliveryMethod.DELIVERY, true, user);

        Mockito.when(timeSlotService.bookTimeSlot(1L, DeliveryMethod.DELIVERY)).thenReturn(timeSlot);

        String expectedJson = "{"
                + "\"id\":1,"
                + "\"startTime\":\"2024-06-16T13:00:00\","
                + "\"endTime\":\"2024-06-16T14:00:00\","
                + "\"deliveryMethod\":\"" + DeliveryMethod.DELIVERY + "\" ,"
                + "\"booked\":true,"
                + "\"bookedByUserId\":\"01\","
                + "\"_links\":{\"self\":{\"href\":\"http://localhost/api/time-slots/1/DELIVERY/book?userId=01\"}}"
        + "}";

        mvc.perform(post("/api/time-slots/1/DELIVERY/book")
                        .param("userId", "01")
                        .with(jwt().jwt(jwt -> jwt.claim("scope", "write"))))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaTypes.HAL_JSON))
                .andExpect(content().json(expectedJson));
    }

    @Test
    public void testGetAvailableTimeSlotsUnauthenticated() throws Exception {
        mvc.perform(get("/api/time-slots")
                        .param("deliveryMethod", "DRIVE")
                        .param("page", "0")
                        .param("size", "2"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void testBookTimeSlotUnauthenticated() throws Exception {
        mvc.perform(post("/api/time-slots/1/DELIVERY/book")
                        .param("userId", "user1"))
                .andExpect(status().isUnauthorized());
    }
}