package com.carrefour.demo.delivery;

import com.carrefour.demo.delivery.config.RsaKeyConfigProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@EnableConfigurationProperties(RsaKeyConfigProperties.class)
@SpringBootApplication
public class DriveAndDeliverApplication {

	public static void main(String[] args) {
		SpringApplication.run(DriveAndDeliverApplication.class, args);
	}



}