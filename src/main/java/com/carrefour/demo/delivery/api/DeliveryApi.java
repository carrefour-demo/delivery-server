package com.carrefour.demo.delivery.api;

import com.carrefour.demo.delivery.application.DeliveryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.Link;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping("/api/delivery")
public class DeliveryApi {

    private final DeliveryService deliveryService;

    @Autowired
    public DeliveryApi(DeliveryService deliveryService) {
        this.deliveryService = deliveryService;
    }

    @GetMapping("/methods")
    public CollectionModel<String> getAllDeliveryMethods() {
        List<String> deliveryMethods = deliveryService.getAllDeliveryMethodNames();

        Link selfLink = linkTo(methodOn(DeliveryApi.class).getAllDeliveryMethods()).withSelfRel();

        return CollectionModel.of(deliveryMethods, selfLink);
    }

}
