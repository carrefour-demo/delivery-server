package com.carrefour.demo.delivery.api;

import com.carrefour.demo.delivery.application.TimeSlotService;
import com.carrefour.demo.delivery.application.data.TimeSlotDTO;
import com.carrefour.demo.delivery.core.delivery.DeliveryMethod;
import com.carrefour.demo.delivery.core.delivery.TimeSlot;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/time-slots")
public class TimeSlotApi {


    private final TimeSlotService timeSlotService;


    @Autowired
    TimeSlotApi(TimeSlotService timeSlotService) {
    this.timeSlotService = timeSlotService;
    }

    @GetMapping
    public List<EntityModel<TimeSlotDTO>> getAvailableTimeSlots(@RequestParam DeliveryMethod deliveryMethod, @RequestParam int page, @RequestParam int size) {
        Page<TimeSlot> timeSlotsPage = timeSlotService.getAvailableTimeSlots(deliveryMethod, PageRequest.of(page, size));
        return timeSlotsPage.getContent().stream()
                .map(timeSlot -> {
                    TimeSlotDTO timeSlotDTO = new TimeSlotDTO(timeSlot.getId(), timeSlot.getStartTime(), timeSlot.getEndTime(), timeSlot.getDeliveryMethod(), timeSlot.isBooked(), timeSlot.getBookedBy() != null ? timeSlot.getBookedBy().getUserId() : null);
                    return EntityModel.of(timeSlotDTO,
                            WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(TimeSlotApi.class).getAvailableTimeSlots(deliveryMethod, page, size)).withSelfRel());})
                .collect(Collectors.toList());
    }

    @PostMapping("/{timeSlotId}/{deliveryMethod}/book")
    public EntityModel<TimeSlotDTO> bookTimeSlot(@PathVariable Long timeSlotId, @PathVariable DeliveryMethod deliveryMethod) {

        TimeSlot bookedTimeSlot = timeSlotService.bookTimeSlot(timeSlotId, deliveryMethod);

        TimeSlotDTO timeSlotDTO = new TimeSlotDTO(bookedTimeSlot.getId(), bookedTimeSlot.getStartTime(), bookedTimeSlot.getEndTime(), bookedTimeSlot.getDeliveryMethod(), bookedTimeSlot.isBooked(), bookedTimeSlot.getBookedBy() != null ? bookedTimeSlot.getBookedBy().getUserId() : null);
        return EntityModel.of(timeSlotDTO,
                WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(TimeSlotApi.class).bookTimeSlot(timeSlotId,deliveryMethod)).withSelfRel());
    }



}

