package com.carrefour.demo.delivery.config;

import com.carrefour.demo.delivery.core.delivery.TimeSlot;
import com.carrefour.demo.delivery.core.delivery.TimeSlotInitializer;
import com.carrefour.demo.delivery.core.delivery.TimeSlotRepository;
import com.carrefour.demo.delivery.core.user.User;
import com.carrefour.demo.delivery.core.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DataLoader implements CommandLineRunner {


    private final TimeSlotRepository timeSlotRepository;

    private final UserRepository userRepository;

    private final BCryptPasswordEncoder passwordEncoder;

    @Value("${time-slot-duration.minutes}")
    private int timeSlotDurationMinutes;
    @Autowired
    public DataLoader(TimeSlotRepository timeSlotRepository, UserRepository userRepository, BCryptPasswordEncoder passwordEncoder) {
        this.timeSlotRepository = timeSlotRepository;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }


    @Override
    public void run(String... args) throws Exception {

        List<TimeSlot> timeSlots = TimeSlotInitializer.initializeTimeSlotsByWeek(this.timeSlotDurationMinutes);
        this.timeSlotRepository.saveAll(timeSlots);


        User user = new User();
        user.setUsername("test");
        user.setEmail("example@gmail.com");
        user.setPassword(this.passwordEncoder.encode("0000"));
        User savedUser = this.userRepository.save(user);

        /** logging  the created user for demo purposes **/

        System.out.println(savedUser);

    }


}