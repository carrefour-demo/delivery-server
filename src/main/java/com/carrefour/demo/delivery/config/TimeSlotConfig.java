package com.carrefour.demo.delivery.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Getter
@Configuration
public class TimeSlotConfig {

    @Value("${time-slot-duration.minutes}")
    private int timeSlotDurationMinutes;

}