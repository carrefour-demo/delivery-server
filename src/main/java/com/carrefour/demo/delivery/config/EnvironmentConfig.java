package com.carrefour.demo.delivery.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Getter
@Configuration
public class EnvironmentConfig {

    @Value("${demo-delivery.dev-url}")
    private String devUrl;

    @Value("${demo-delivery.prod-url}")
    private String prodUrl;
}
