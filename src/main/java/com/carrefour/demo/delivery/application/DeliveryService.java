package com.carrefour.demo.delivery.application;

import com.carrefour.demo.delivery.core.delivery.DeliveryMethod;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DeliveryService {

    public List<String> getAllDeliveryMethodNames() {
        return Arrays.stream(DeliveryMethod.values())
                .map(Enum::name)
                .collect(Collectors.toList());
    }
}
