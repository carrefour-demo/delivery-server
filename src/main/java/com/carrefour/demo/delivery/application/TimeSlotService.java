package com.carrefour.demo.delivery.application;

import com.carrefour.demo.delivery.core.delivery.DeliveryMethod;
import com.carrefour.demo.delivery.core.delivery.TimeSlot;
import com.carrefour.demo.delivery.core.delivery.TimeSlotRepository;
import com.carrefour.demo.delivery.core.service.JpaUserDetailsService;
import com.carrefour.demo.delivery.core.user.AuthUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Collections;

@Service
public class TimeSlotService {
    private final TimeSlotRepository timeSlotRepository;

    private final JpaUserDetailsService jpaUserDetailsService;
    TimeSlotService(TimeSlotRepository timeSlotRepository, JpaUserDetailsService jpaUserDetailsService) {
        this.timeSlotRepository = timeSlotRepository;
        this.jpaUserDetailsService = jpaUserDetailsService;
    }


    public Page<TimeSlot> getAvailableTimeSlots(DeliveryMethod deliveryMethod, Pageable pageable) {
        switch (deliveryMethod) {
            case DRIVE:
                return Page.empty();
            case DELIVERY:
                return timeSlotRepository.findByBookedFalseOrderByStartTimeAsc(pageable);
            case DELIVERY_TODAY:
                LocalDateTime startOfToday = LocalDateTime.of(LocalDate.now(), LocalTime.MIN);
                LocalDateTime endOfToday = LocalDateTime.of(LocalDate.now(), LocalTime.MAX);
                return timeSlotRepository.findByBookedFalseAndStartTimeBetweenOrderByStartTimeAsc(startOfToday, endOfToday, pageable);
            case DELIVERY_ASAP:
                return timeSlotRepository.findFirstByBookedFalseOrderByStartTimeAsc()
                        .map(timeSlot -> new PageImpl<>(Collections.singletonList(timeSlot), pageable, 1))
                        .orElseGet(() -> new PageImpl<>(Collections.emptyList(), pageable, 0));
            default:
                return timeSlotRepository.findByBookedFalseOrderByStartTimeAsc(pageable);
        }
    }

    public TimeSlot bookTimeSlot(Long timeSlotId, DeliveryMethod deliveryMethod) {


        TimeSlot timeSlot = timeSlotRepository.findById(timeSlotId).orElseThrow(() -> new RuntimeException("Time Slot not found"));
        if (timeSlot.isBooked()) {
            throw new RuntimeException("Time Slot already booked");
        }
        AuthUser user = (AuthUser) jpaUserDetailsService.loadUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        timeSlot.setBooked(true);
        timeSlot.setDeliveryMethod(deliveryMethod);
        timeSlot.setBookedBy(user.getUser());
        return timeSlotRepository.save(timeSlot);
    }


}