package com.carrefour.demo.delivery.application.data;

import com.carrefour.demo.delivery.core.delivery.DeliveryMethod;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class TimeSlotDTO {
    private Long id;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private DeliveryMethod deliveryMethod;
    private boolean booked;
    private String bookedByUserId;

    public TimeSlotDTO(Long id, LocalDateTime startTime, LocalDateTime endTime, DeliveryMethod deliveryMethod, boolean booked, String bookedByUserId) {
        this.id = id;
        this.startTime = startTime;
        this.endTime = endTime;
        this.deliveryMethod = deliveryMethod;
        this.booked = booked;
        this.bookedByUserId = bookedByUserId;
    }

}
