package com.carrefour.demo.delivery.core.delivery;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Optional;
@Repository
public interface TimeSlotRepository extends JpaRepository<TimeSlot, Long> {
    Page<TimeSlot> findByBookedFalseOrderByStartTimeAsc(Pageable pageable);
    Page<TimeSlot> findByBookedFalseAndStartTimeBetweenOrderByStartTimeAsc(LocalDateTime start, LocalDateTime end, Pageable pageable);
    Optional<TimeSlot> findFirstByBookedFalseOrderByStartTimeAsc();

}