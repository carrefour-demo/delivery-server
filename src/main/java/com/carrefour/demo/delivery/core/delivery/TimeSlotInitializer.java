package com.carrefour.demo.delivery.core.delivery;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;
@Component
public  class TimeSlotInitializer {

    public static List<TimeSlot> initializeTimeSlotsByWeek(int timeSlotDurationMinutes) {

        List<TimeSlot> timeSlots = new ArrayList<>();
        LocalDate today = LocalDate.now();
        LocalTime startTime = LocalTime.of(8, 0);
        LocalTime endTime = LocalTime.of(17, 0);

        for (int dayOffset = 0; dayOffset < 7; dayOffset++) {
            LocalDate currentDate = today.plusDays(dayOffset);
            LocalTime currentTime = startTime;

            while (currentTime.isBefore(endTime)) {
                LocalDateTime startDateTime = LocalDateTime.of(currentDate, currentTime);
                LocalDateTime endDateTime = startDateTime.plusMinutes(timeSlotDurationMinutes);
                if (endDateTime.toLocalTime().isAfter(endTime)) {
                    break;
                }

                TimeSlot timeSlot = new TimeSlot();
                timeSlot.setStartTime(startDateTime);
                timeSlot.setEndTime(endDateTime);
                timeSlot.setDeliveryMethod(null);
                timeSlot.setBooked(false);
                timeSlot.setBookedBy(null);  // No user has booked this slot initially

                timeSlots.add(timeSlot);

                currentTime = currentTime.plusMinutes(timeSlotDurationMinutes);
            }
        }

        return timeSlots;
    }
}