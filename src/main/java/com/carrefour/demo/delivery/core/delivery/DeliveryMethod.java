package com.carrefour.demo.delivery.core.delivery;

public enum DeliveryMethod {
    DRIVE,
    DELIVERY,
    DELIVERY_TODAY,
    DELIVERY_ASAP
}
